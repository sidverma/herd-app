//
//  UserManager.swift
//  Herd
//
//  Created by Sid Verma on 11/29/18.
//  Copyright © 2018 Sid Verma. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore
import PromiseKit
import CodableFirebase

protocol UserManager: LocationManager {
    
}

extension UserManager {
    
    /**
     - Promise Kit Wrapper for getUser
     - Calls: getUser(id: String, user_: @escaping (User?,Error?) -> Void)
    */
    public func getUser(id:String) -> Promise<User> {
        return Promise {seal in
            getUser(id: id, { (user, error) in
                seal.resolve(user, error)
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for signIn
     - Calls: signIn(_ completion: @escaping (Error?) -> Void)
     */
    public func signIn() -> Promise<Void> {
        return Promise {seal in
            signIn({ (error) in
                seal.resolve(error)
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for signing a user up
     - Calls: createUser(id: String, icon: String,name: String, _ completion: @escaping (User?,Error?) -> Void)
     */
    public func signUp(id: String, icon: String, name: String) -> Promise<User> {
        let name = "Chill Debug Kuala"
        return Promise {(seal) in
            createUser(id: id, icon: icon, name: name, { (user, error) in
                seal.resolve(user, error)
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for getting all posts a user has upvoted
     */
    public func getUpvotes() {}
    
    /**
     - Promise Kit Wrapper for getting all posts a user has downvoted
    */
    public func getDownvotes(){}
    
    /**
     - Promise Kit Wrapper for getting all posts a user has commented on and the according comments
     */
    public func getComments(){}
}

extension UserManager {
    
    /**
     - Signs user in and return completion block with error
     */
    private func signIn(_ completion: @escaping (Error?) -> Void){
        Auth.auth().signInAnonymously {(authResult, error) in
            guard let user = authResult?.user else {
                //throw a fat error
                completion(error)
                return
            }
            
            let isAnon  = user.isAnonymous
            let uid     = user.uid
            
            //If this is a new user
            if UserDefaults.standard.string(forKey: "id") == nil {
                self.signUp(id: uid, icon: "some icon", name: "some name").done({ (user) in
                  UserDefaults.standard.set(uid, forKey: "id")
                }).catch({ (error) in
                    completion(error)
                })
            }
            
            print("This user is anonymous \(isAnon) and the user id \(uid)")
            completion(error)
        }
    }
    
    /**
     - Given an id, icon url, and name this will create and store a user object
     */
    private func createUser(id: String, icon: String,name: String, _ completion: @escaping (User?,Error?) -> Void) {
        let db      = Firestore.firestore()
        let myRef   = db.collection("users").document(id)
        let encoder = FirestoreEncoder()
        let name    = name
        let icon    = icon
        
        getGeohash().done { (geohash) in
            let myUser = User(id: id, name: name, icon: icon, location: geohash)
            
            do {
                
                let postData    = try encoder.encode(myUser)
                
                myRef.setData(postData, completion: { (error) in
                    completion(myUser,error)
                })
                
            } catch let error {
                
                completion(nil,error)
            }
            
            }.catch { (error) in
                completion(nil,error)
        }
        
    }
    
    /**
     - Given a user id this will return a completion block with either the user or an error
     */
    private func getUser(id: String, _ completion: @escaping (User?,Error?) -> Void) {
        guard !id.isEmpty else {
            let error = NSError(domain: "Invalid-Id", code: -1, userInfo: [NSLocalizedDescriptionKey: "user id literally empty bro and that's way no ok"])
            completion(nil,error)
            return
        }
        let db          = Firestore.firestore()
        let userRef     = db.collection("users").document(id)
        let decoder     = FirestoreDecoder()
        
        userRef.getDocument { (documentSnap, error) in
            guard let data = documentSnap?.data() else {
                completion(nil,error)
                return
            }
            
            do {
                
                let user   = try decoder.decode(User.self, from: data)
                completion(user,nil)
                
            } catch let error {
                
                completion(nil,error)
                
            }
            
        }
    }

}
