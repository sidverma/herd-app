//
//  PostManager.swift
//  Herd
//
//  Created by Sid Verma on 11/29/18.
//  Copyright © 2018 Sid Verma. All rights reserved.
//

import Foundation
import FirebaseFirestore
import CodableFirebase
import PromiseKit

protocol PostManager: UserManager {
    
}

//All private PostManager functions
extension PostManager {
    /**
     - Get all posts near me
     */
    private func getPosts(_ completion: @escaping ([Post]?,Error?) -> Void) {
        let db                      = Firestore.firestore()
        let decoder                 = FirestoreDecoder()
        let postsCollection         = db.collection("posts")
        let startingTimstamp        = Timestamp.init(date: Date.twentyFourHoursAgo)
        let maxDelta                = -5
        let timeFilteredPostRef     = postsCollection.whereField("timestamp", isGreaterThan: startingTimstamp)
        //let deltaFilteredPostRef    = timeFilteredPostRef.whereField("delta", isGreaterThan: maxDelta)
        
        getGeohash()
            
            .done{(geoHash) in
            
                let postFilteredRef = timeFilteredPostRef.whereField("location", isEqualTo: geoHash)
                
                
                postFilteredRef.getDocuments(completion: { (querySnapshot, error) in
                    
                    guard let documents = querySnapshot?.documents else {
                        completion(nil,error)
                        return
                    }
                    
                    var postList    = [Post]()
                    
                    for document in documents {
                        
                        do {
                            
                            let post    = try decoder.decode(Post.self, from: document.data())
                            if post.delta < maxDelta {postList.append(post)}
                            
                        } catch let error {
                            
                            //To-Do throw Error
                            print(error)
                            
                            
                        }
                        
                    }
                    
                    completion(postList,nil)
                    
                })
                
                
            
            }.catch{(error) in
            //Error in getting geohash!
                print("Error in getting geohash!: \(error.localizedDescription)")
        }
    }
    
    /**
    -   Given a post id this function will return either an error to be handled or a post optional
     */
    private func getPost(id: String, post_:@escaping (Post?,Error?) -> Void) {
        let db      = Firestore.firestore()
        let postRef = db.collection("posts").document(id)
        let decoder = FirestoreDecoder()
        
        postRef.getDocument { (documentSnap, error) in
            guard let data = documentSnap?.data() else {
                //To-Do throw Error
                post_(nil,error)
                return
            }
            
            do {
                
                let post    = try decoder.decode(Post.self, from: data)
                post_(post,nil)
                
            } catch let error {

                //To-Do throw Error
                print(error)
                post_(nil,error)

            }
            
        }
    }
    
    /**
    -   Given some text create a post
     */
    private func createPost(text: String, _ completion:@escaping (Post?, Error?) -> Void) {
        guard let id        = UserDefaults.standard.string(forKey: "id") else {return}
        let db              = Firestore.firestore()
        let postRef         = db.collection("posts").document()
        let postID          = postRef.documentID
        let upvotes         = 0
        let downvotes       = 0
        let delta           = 0
        
        when(fulfilled: getUser(id: id), getGeohash()).done { (user,location) in
            
            let post = Post(id: postID, text: text, user: user, upvotes: upvotes, downvotes: downvotes, delta: delta, comments: nil, votes: nil, location: location)
            
            self.writePost(post).done({ (post) in
                completion(post, nil)
            }).catch({ (error) in
                completion(nil,error)
            })
            
            }.catch { (error) in
                completion(nil,error)
        }
        
    }
    
    /**
     -   Given a post, write to the database and return completion
     */
    private func writePost(_ post: Post, completion: @escaping (Post,Error?) -> Void){
        let db              = Firestore.firestore()
        let postRef         = db.collection("posts").document(post.id)
        
        do {
            let postData    = try FirestoreEncoder().encode(post)
            postRef.setData(postData) { (error) in
                completion(post,error)
            }
        } catch let error {
                completion(post,error)
        }
        
    }
    
}

private typealias PostManagerPromiseKit = PostManager

//All public PostManager Promise Kit Wrappers
extension PostManagerPromiseKit {
    /**
     -   Promise Kit wrapper for getPosts
     */
    public func getPosts() -> Promise<[Post]>{
        return Promise { seal in
            getPosts({ (postList, error) in
                seal.resolve(postList, error)
            })
        }
    }
    
    /**
     -   Promise Kit wrapper for getPost
     */
    public func getPost(id:String) -> Promise<Post> {
        return Promise { seal in
            getPost(id: id, post_: { (post, error) in
                seal.resolve(post, error)
            })
        }
    }
    
    /**
     -  Promise Kit wrapper for write post
     */
    public func createPost(text:String) -> Promise<Post> {
        return Promise {seal in
            createPost(text: text, { (post, error) in
                seal.resolve(post, error)
            })
        }
    }
    
    /**
     -   Promise Kit wrapper for write post
     */
    private func writePost(_ post : Post) -> Promise<Post> {
        return Promise { seal in
            writePost(post, completion: { (post,error) in
                seal.resolve(post, error)
            })
        }
    }
    
    
}



private typealias PostPromiseKit = Post



//TEST BENCH CLASS
class testBench: UIViewController, UserManager, PostManager {
    
    @IBAction func signInTapped(_ sender: Any) {
        
        signIn().done { () in
            print("Sign In Worked!")
            }.catch { (error) in
                print("Sign in failed with: \(error)")
        }
        
    }
    
    @IBAction func createPostedTapped(_ sender: Any) {
        
        createPost(text: "Testing!").done { (post) in
                print("Post successfully created!")
            }.catch { (error) in
                print("Creating Post Failed with: \(error)")
        }
        
    }
    
    
    @IBAction func getPosts(_ sender: Any) {
        
        getPosts().done { (postList) in
            print("Fetched Posts Worked, there are \(postList.count) posts")
            }.catch {(error) in
                print("Fetched Posts failed with: \(error)")
        }
        
    }
    
    
    override func viewDidLoad() {
        
        
    }
    
}
