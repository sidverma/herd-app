//
//  Post.swift
//  Herd
//
//  Created by Sid Verma on 11/29/18.
//  Copyright © 2018 Sid Verma. All rights reserved.
//

import Foundation
import FirebaseFirestore
import CodableFirebase
import PromiseKit

public typealias Comment = Post

/**
 Protocol for posts
 */
private protocol PostProtocol: Codable {
    var id         : String         {get}
    var text       : String         {get}
    var user       : User           {get}
    var upvotes    : Int            {get}
    var downvotes  : Int            {get}
    var comments   : [Comment]?     {get}
    var votes      : [Vote]?        {get}
    var delta      : Int            {get}
    var location   : String         {get}
    var timestamp  : Timestamp      {get}
}

/**
 Post class
 */
public class Post: PostProtocol, LocationManager, PostManager, Codable {
    
    var id          : String
    var text        : String
    var user        : User
    var upvotes     : Int
    var downvotes   : Int
    var delta       : Int
    var comments    : [Comment]?
    var votes       : [Vote]?
    var location    : String
    var timestamp   : Timestamp
    
    init (id: String, text: String, user: User, upvotes: Int, downvotes: Int, delta: Int, comments: [Comment]?, votes: [Vote]?, location: String) {
        self.id         = id
        self.text       = text
        self.user       = user
        self.upvotes    = upvotes
        self.downvotes  = downvotes
        self.delta      = delta
        self.comments   = comments
        self.votes      = votes
        self.location   = location
        self.timestamp  = Timestamp.init()
    }
    
    
}

//All private post functions
extension Post {
    
    /**
     - Upvote the post in a transcation
     */
    private func upvote(_ completion:@escaping (Error?) -> Void) {
        guard let id                = UserDefaults.standard.string(forKey: "id") else {return}
        let post                    = self
        let db                      = Firestore.firestore()
        let decoder                 = FirestoreDecoder()
        let encoder                 = FirestoreEncoder()
        let postsCollection         = db.collection("posts")
        let postRef                 = postsCollection.document(post.id)
        let myVoteRef               = db.collection("users").document(id).collection("voted").document(post.id)
        let postVoteRef             = postRef.collection("votes").document(id)
        
        getUser(id: id).done {(user) in
            //Start db transcation
            db.runTransaction({ (transaction, errorPointer) -> Any? in
                let document: DocumentSnapshot
                let voteDoc : DocumentSnapshot
                //Get post document
                do {
                    try document = transaction.getDocument(postRef)
                    try voteDoc  = transaction.getDocument(postVoteRef)
                    //Catch errors
                } catch let fetchError as NSError {
                    errorPointer?.pointee = fetchError
                    completion(fetchError)
                    return nil
                }
                
                //Check if document has data otherwise throw error
                guard let postData  = document.data() else {
                    let error = NSError(domain: "DataError", code: -1, userInfo: [NSLocalizedDescriptionKey: "Document \(document.documentID) does not have any data"])
                    completion(error)
                    return nil
                }
                
                
                
                //Error handling for decoding Post json data
                do {
                    
                    let post_       = try decoder.decode(Post.self, from: postData)
                    
                    if voteDoc.exists {
                        //Has been voted on in someway
                        guard let voteData  = voteDoc.data() else {
                            return nil
                        }
                        
                        do {
                            let vote    = try decoder.decode(Vote.self, from: voteData)
                            
                            if !vote.isUpvoted {
                                //Post was downvoted - clear downvote and add upvote
                                let vote       = Vote(id: id, user: user, isUpvoted: true)
                                let vote_       = try encoder.encode(vote)
                                let upvotes     = post_.upvotes + 1
                                let downvotes   = post_.downvotes - 1
                                let delta       = upvotes - downvotes
                                
                                //Write downvote in a transcation
                                transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                                transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                                transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                                transaction.setData     (postData                   , forDocument   : myVoteRef)
                                transaction.setData     (vote_                      ,forDocument   : postVoteRef)
                                
                                //Update self with new values
                                post.upvotes    = upvotes
                                post.downvotes  = downvotes
                                post.delta      = delta
                                
                            } else if vote.isUpvoted {
                                //Post was upvoted - clear upvote
                                let upvotes     = post_.upvotes - 1
                                let downvotes   = post_.downvotes
                                let delta       = upvotes - downvotes
                                transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                                transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                                transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                                transaction.deleteDocument(myVoteRef)
                                transaction.deleteDocument(postVoteRef)
                                
                            }
                        } catch let error {
                            completion(error)
                        }
                        
                    } else {
                        //No vote has been caste yet - add downvote
                        let vote       = Vote(id: id, user: user, isUpvoted: true)
                        let vote_       = try encoder.encode(vote)
                        let upvotes     = post_.upvotes + 1
                        let downvotes   = post_.downvotes
                        let delta       = upvotes - downvotes
                        
                        //Write upvote in a transcation
                        transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                        transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                        transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                        transaction.setData     (postData                   , forDocument   : myVoteRef)
                        transaction.setData     (vote_                      ,forDocument   : postVoteRef)
                        
                        
                        
                        //Update self with new values
                        post.upvotes    = upvotes
                        post.delta      = delta
                        post.downvotes  = downvotes
                        
                    }
                    
                    
                    
                    return nil
                    
                    
                } catch let error {
                    
                    completion(error)
                    
                }
                
                //Assuming no return from do or catch block return no error
                return nil
                
            }) { (object, error) in
                if let error = error {
                    print("Transaction failed: \(error)")
                    completion(error)
                } else {
                    print("Transaction successfully committed!")
                    completion(nil)
                }
            }
            
            
            }.catch { (error) in
                completion(error)
        }
    }
    
    /**
     - Downvote the post in a transcation
     */
    private func downvote(_ completion:@escaping (Error?) -> Void) {
        guard let id                = UserDefaults.standard.string(forKey: "id") else {return}
        let post = self
        let db                      = Firestore.firestore()
        let decoder                 = FirestoreDecoder()
        let encoder                 = FirestoreEncoder()
        let postsCollection         = db.collection("posts")
        let postRef                 = postsCollection.document(post.id)
        let myVoteRef               = db.collection("users").document(id).collection("voted").document(post.id)
        let postVoteRef             = postRef.collection("votes").document(id)
        
        getUser(id: id).done {(user) in
            //Start db transcation
            db.runTransaction({ (transaction, errorPointer) -> Any? in
                let document: DocumentSnapshot
                let voteDoc : DocumentSnapshot
                //Get post document
                do {
                    try document = transaction.getDocument(postRef)
                    try voteDoc  = transaction.getDocument(postVoteRef)
                    //Catch errors
                } catch let fetchError as NSError {
                    errorPointer?.pointee = fetchError
                    completion(fetchError)
                    return nil
                }
                
                //Check if document has data otherwise throw error
                guard let postData  = document.data() else {
                    let error = NSError(domain: "DataError", code: -1, userInfo: [NSLocalizedDescriptionKey: "Document \(document.documentID) does not have any data"])
                    completion(error)
                    return nil
                }
                
                

                //Error handling for decoding Post json data
                do {
                    
                    let post_       = try decoder.decode(Post.self, from: postData)
                    
                    if voteDoc.exists {
                        //Has been voted on in someway
                        guard let voteData  = voteDoc.data() else {
                            return nil
                        }
                        
                        do {
                            let vote    = try decoder.decode(Vote.self, from: voteData)
                            
                            if vote.isUpvoted {
                                //Post was upvoted - clear upvote and add downvote
                                let vote       = Vote(id: id, user: user, isUpvoted: false)
                                let vote_       = try encoder.encode(vote)
                                let upvotes     = post_.upvotes - 1
                                let downvotes   = post_.downvotes + 1
                                let delta       = upvotes - downvotes
                                
                                //Write downvote in a transcation
                                transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                                transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                                transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                                transaction.setData     (postData                   , forDocument   : myVoteRef)
                                transaction.setData     (vote_                      ,forDocument   : postVoteRef)
                                
                                //Update self with new values
                                post.upvotes    = upvotes
                                post.downvotes  = downvotes
                                post.delta      = delta
                                
                            } else if !vote.isUpvoted {
                                //Post was downvoted - clear downvote
                                let upvotes     = post_.upvotes
                                let downvotes   = post_.downvotes - 1
                                let delta       = upvotes - downvotes
                                transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                                transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                                transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                                transaction.deleteDocument(myVoteRef)
                                transaction.deleteDocument(postVoteRef)
                                
                            }
                        } catch let error {
                            completion(error)
                        }
                        
                    } else {
                        //No vote has been caste yet - add downvote
                        let vote       = Vote(id: id, user: user, isUpvoted: false)
                        let vote_       = try encoder.encode(vote)
                        let upvotes     = post_.upvotes
                        let downvotes   = post_.downvotes + 1
                        let delta       = upvotes - downvotes
                        
                        //Write upvote in a transcation
                        transaction.updateData  (["upvotes": upvotes]       , forDocument   : postRef)
                        transaction.updateData  (["downvotes": downvotes]   , forDocument   : postRef)
                        transaction.updateData  (["delta": delta]           , forDocument   : postRef)
                        transaction.setData     (postData                   , forDocument   : myVoteRef)
                        transaction.setData     (vote_                      ,forDocument   : postVoteRef)
                        
                        
                        
                        //Update self with new values
                        post.upvotes    = upvotes
                        post.delta      = delta
                        post.downvotes  = downvotes
                        
                    }
                    
                    
                    
                    return nil
                    
                    
                } catch let error {
                    
                    completion(error)
                    
                }
                
                //Assuming no return from do or catch block return no error
                return nil
                
            }) { (object, error) in
                if let error = error {
                    print("Transaction failed: \(error)")
                    completion(error)
                } else {
                    print("Transaction successfully committed!")
                    completion(nil)
                }
            }
            
            
            }.catch { (error) in
                completion(error)
        }
    }
    
    /**
     - Add a comment to the post
     */
    private func addComment(_ text: String,_ completion:@escaping (Comment?,Error?) -> Void) {
        
        guard let id                = UserDefaults.standard.string(forKey: "id") else {return}
        let post                    = self
        let db                      = Firestore.firestore()
        let userCommmentsCollection = db.collection("user").document(id).collection("comments").document(post.id).collection("comments").document()
        let commentID               = userCommmentsCollection.documentID
        let upvotes                 = 0
        let downvotes               = 0
        let delta                   = 0
        
        //Write comment using PromiseKit
        when(fulfilled: getUser(id: id), getGeohash()).done {[weak self] (user,location) in
            
            //Create comment
            let comment = Comment(id: commentID, text: text, user: user, upvotes: upvotes, downvotes: downvotes, delta: delta, comments: nil, votes: nil, location: location)
            
            //Write comment and complete
            self?.writeComment(comment: comment).done({ (comment) in
                completion(comment,nil)
            
            //Catch errors
            }).catch({ (error) in
                completion(nil, error)
            })
            
            //Catch errors
            }.catch { (error) in
                completion(nil,error)
        }
    }
    
    
    /**
     - Given a comment write the comment to database
     */
    private func writeComment(comment: Comment, _ completion: @escaping (Comment?, Error?) -> Void) {
        guard let id                = UserDefaults.standard.string(forKey: "id") else {
            let error = NSError(domain: "Invalid-Id", code: -1, userInfo: [NSLocalizedDescriptionKey: "user id does not exist in user defaults"])
            completion(nil,error)
            return
        }
        let post                    = self
        let db                      = Firestore.firestore()
        let encoder                 = FirestoreEncoder()
        let postsCollection         = db.collection("posts")
        let commentRef              = postsCollection.document(post.id).collection("comments").document(comment.id)
        let userCommmentsRef        = db.collection("user").document(id).collection("comments").document(post.id).collection("comments").document()
        
        //Encode comment model to JSON data, if able to encode write data
        do {
            let commentData    = try encoder.encode(comment)
            
            commentRef          .setData(commentData)
            userCommmentsRef    .setData(commentData)
            completion          (comment,nil)
            
            
        } catch let error {
            completion(nil,error)
        }
        
    }
    
}

//All public Post Promise Kit Wrappers
extension Post {
    /**
     - Promise Kit Wrapper for upvote
     - Calls: upvote(_ completion:@escaping (Error?) -> Void)
     */
    public func upvote() -> Promise<Void> {
        return Promise {(seal) in
            upvote({ (error) in
                seal.resolve(error)
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for downvote
     - Calls: downvote(_ completion:@escaping (Error?) -> Void)
     */
    public func downvote() -> Promise<Void> {
        return Promise {(seal) in
            downvote({ (error) in
                seal.resolve(error)
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for adding comments
     - Calls: addComment(_ text: String,_ completion:@escaping (Comment?,Error?) -> Void)
     */
    public func addComment(_ text: String) -> Promise<Comment> {
        return Promise {(seal) in
            addComment(text, { (comment, error) in
                seal.resolve(comment, error)
                
            })
        }
    }
    
    /**
     - Promise Kit Wrapper for writing comments
     - Calls: writeComment(comment: Comment, _ completion: @escaping (Comment?, Error?) -> Void)
     */
    private func writeComment(comment: Comment) -> Promise<Comment>{
        return Promise {(seal) in
            writeComment(comment: comment, { (comment, error) in
                seal.resolve(comment, error)
            })
        }
    }
    
}
