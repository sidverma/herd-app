//
//  User.swift
//  Herd
//
//  Created by Sid Verma on 11/29/18.
//  Copyright © 2018 Sid Verma. All rights reserved.
//

import Foundation



private protocol UserProtocol: Codable {
    var id          : String        {get}
    var name        : String        {get}
    var icon        : String        {get}
    var posts       : [Post]?       {get}
    var score       : Int           {get}
    var location    : String        {get}
    var votes       : [Vote]?       {get}
    var comments    : [Comment]?    {get}
}

public class User: UserProtocol,Codable, LocationManager {

    var id          : String
    var name        : String
    var icon        : String
    var posts       : [Post]?
    var score       : Int
    var location    : String
    var votes       : [Vote]?
    var comments    : [Comment]?
    
    //New user
    init(id: String,name:String,icon:String,location: String) {
        self.id         = id
        self.name       = name
        self.icon       = icon
        self.location   = location
        self.score      = 0
    }
 
    
}
