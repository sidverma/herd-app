//
//  Vote.swift
//  Herd
//
//  Created by Sid Verma on 2/15/19.
//  Copyright © 2019 Sid Verma. All rights reserved.
//

import Foundation
import FirebaseFirestore
import CodableFirebase


/**
 Protocol for vote
 */
private protocol VoteProtocol: Codable {
    var id          : String        {get}
    var user        : User          {get}
    var isUpvoted   : Bool          {get}
    var timestamp   : Timestamp     {get}
}

public class Vote : VoteProtocol {
    
    var id          : String
    var user        : User
    var isUpvoted   : Bool
    var timestamp   : Timestamp
    
    init (id: String,user: User, isUpvoted: Bool) {
        self.id         = id
        self.user       = user
        self.isUpvoted  = isUpvoted
        self.timestamp  = Timestamp.init()
    }
    
}
