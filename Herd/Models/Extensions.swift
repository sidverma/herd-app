//
//  Extensions.swift
//  Herd
//
//  Created by Sid Verma on 11/30/18.
//  Copyright © 2018 Sid Verma. All rights reserved.
//

import Foundation
import FirebaseFirestore
import CodableFirebase

extension DocumentReference  : DocumentReferenceType {}
extension GeoPoint          : GeoPointType {}
extension FieldValue        : FieldValueType {}
extension Timestamp         : TimestampType {}


/**
 Extends Date for convenient time management
 */
extension Date {
    static var twentyFourHoursAgo: Date {
        return Calendar.current.date(byAdding: .hour,value: -24, to: Date())!
    }
    static var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date().noon)!
    }
    static var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date().noon)!
    }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
